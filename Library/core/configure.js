/*!
// Testweb v0.3.0
// http://or-change.cn
// Copyright 2014 OrChange Inc. All rights reserved.
// Licensed under the GPL License.
*/
/*jslint vars: true, forin: true */
/*global define: false */
/**
 * OC is the core module of the whole project. It is responsible for the configuration of the
 * running environment and load the jQuery module.
 * @module config
 * @requires jQuery
 * @example
 *     var OC = require("./oc"); //How to require OC module.
 */
define(function (require, exports, module) {
	'use strict';
	var DefaultSettings = require("lib/DefaultSettings");
	var LocalSettings = require("LocalSettings");
	var config, default_item, local_item;

	/**
	 * Runtime environment configure.
	 * @class config
	 * @static
	 */
	config = {
		"C_LANGUAGE": "zh_CN",
		"C_HTML_LANG": "zh",
		"C_LANGUAGE_PATH": "Language/",
		"C_SERVER": "/",
		"C_SCRIPT_PATH": "xvc/",
		"C_VIEW_POSTFIX": "_template.html",
		"C_DEFAULT_VIEW": "index",
		/**
		 * ["append"] / "no-action"
		 */
		"C_NO_BOX_MODE": "append",
		/**
		 * ["mix"] / "hash" / "remote"
		 */
		"C_SUBMIT_MODE": "mix",
		"C_CROSS_DOMAIN": false,
		"C_VIEW_JSONP_API": "",
		"C_LANG_JSONP_API": "",
		"C_JSONP": "callback",
		"C_JSONP_CALLBACK": "jsonpCallback"
	};

	for (default_item in DefaultSettings) {
		if (!config.hasOwnProperty(default_item)) {
			config[default_item] = DefaultSettings[default_item];
		}
	}

	for (local_item in LocalSettings) {
		if (config.hasOwnProperty(local_item)) {
			config[local_item] = LocalSettings[local_item];
		}
	}

	exports.get = function (key) {
		if (config.hasOwnProperty(key)) {
			return config[key];
		}
		return false;
	};

	exports.set = function (key, value) {
		if (config.hasOwnProperty(key)) {
			config[key] = value;
			return value;
		}
		return false;
	};

});
